README

Simple Content Access

At the moment there is an 'add story content', 'edit story content' and
'delete story content', but there is no 'view story content', only the 'access content'
permission to grant the role the permission to see all the content types. This simple
module just adds a view permission for each content type.

If a role can access to all the content types, enable the 'access content' core permission,
under the Node permissions section.
